/************************************************ 
Code by Marc Tajiri
*************************************************/

/************************************************ ALU 
*************************************************
*************************************************/
module ALU (s, zero, ovfl, x, y, const_shift, cont);

//inputs and outputs
output [31:0] s;
output zero, ovfl;
input [31:0] x, y;
input [7:0] cont;
input [4:0] const_shift;
wire [31:0] addsub_out, logic_out, shift_out;
wire [4:0] shift_var;
wire c_out;

//modules
lsb_in li(shift_var, x[31], x[30], x[29], x[28], x[27]);
addsub as(addsub_out, c_out, x, y, cont[0]);
mux32bit m32(s, shift_out, addsub_out[0], addsub_out, logic_out, cont[7], cont[6]);
logic_unit LU(logic_out, x, y, cont[5], cont[4]);
shifter_unit SU(shift_out, y, const_shift, shift_var, cont[3], cont[2], cont[1]);
zero_check zc (zero, addsub_out);
Overflow_out overfl (ovfl, c_out, addsub_out[31]);

endmodule

/************************************************ SHIFT LSB IN
************************************************* DONE
*************************************************/
module lsb_in (shift_var, i5, i4, i3, i2, i1);
output [4:0] shift_var;
input i1, i2, i3, i4, i5;
assign shift_var[0] = i1;
assign shift_var[1] = i2;
assign shift_var[2] = i3;
assign shift_var[3] = i4;
assign shift_var[4] = i5;

endmodule

/************************************************ OVERFLOW
************************************************* DONE
*************************************************/
module Overflow_out (ovfl_out, a, b);
output ovfl_out;
input a, b;
xor (ovfl_out, a, b);
endmodule


/************************************************ ZERO OUT
************************************************* DONE
*************************************************/
module zero_check (zero_out, a);
output zero_out;
input [31:0] a;
nor checkZero [31:0] (zero_out, a);
endmodule

/************************************************ LOGIC UNIT
************************************************* DONE
*************************************************/
module logic_unit (log_out, a, b, contr1, contr0);
output [31:0] log_out;
input [31:0] a, b;
input contr0, contr1;
wire [31:0] r0, r1, r2, r3;
and and32b [31:0] (r0, a, b);
or or32b [31:0] (r1, a, b);
xor xor32b [31:0] (r2, a, b);
nor nor32b [31:0] (r3, a, b);
mux32bit LU_out(log_out, r0, r1, r2, r3, contr1, contr0);
endmodule


/************************************************ SHIFTER
************************************************* DONE
*************************************************/
module shifter_unit (shift_out, a, const, var, contr2, contr1, contr0);
output [31:0] shift_out;
input [31:0] a;
input [4:0] const, var;
input contr0, contr1, contr2;
wire [31:0] r0, r1, r2, r3;
wire [4:0] b;
mux2to1_5b shiftermux(b, const, var, contr0);
assign r0 = a;
assign r1 = a << b;
assign r2 = a >> b;
assign r3 = a >>> b;
mux32bit shift_mux(shift_out, r0, r1, r2, r3, contr2, contr1);
endmodule


/************************************************ 32 BIT MUX 
************************************************* DONE
*************************************************/
module mux4_to_1 (out, i0, i1, i2, i3, s1, s0);

//Port Declaration
output out;
input i0, i1, i2, i3;
input s1, s0;

// Wire Declaration
wire s1n, s0n;

// Create s1n and s0n signals
not (s1n, s1);
not (s0n, s0);

// 3-input and gates instatiated
and (y0, i0, s1n, s0n);
and (y1, i1, s1n, s0);
and (y2, i2, s1, s0n);
and (y3, i3, s1, s0);

// 4-input or gate instatiated
or (out, y0, y1, y2, y3);

endmodule


//32 Bit mux
module mux32bit (out, i0, i1, i2, i3, s1, s0);

//Port Declaration
output [31:0] out;
input [31:0] i0, i1, i2, i3;
input s1, s0;

// Wire Declaration
wire s1n, s0n;

mux4_to_1 m1 [31:0] (out, i0, i1, i2, i3, s1, s0);

endmodule

module mux2_to_1 (out, i0, i1, s0);

//Port Declaration
output out;
input i0, i1;
input s0;

// Wire Declaration
wire s0n;

// Create s0n signal
not (s0n, s0);

// 3-input and gates instatiated
and (y0, i0, s0n);
and (y1, i1, s0);

// 3-input or gate instatiated
or (out, y0, y1);

endmodule

module mux2to1_5b (out, i0, i1, s0);

//Port Declaration
output [4:0] out;
input [4:0] i0, i1;
input s0;

// Wire Declaration
wire s0n;

mux2_to_1 m1 [4:0] (out, i0, i1, s0);

endmodule


/************************************************ FULL ADDER/SUBTRACTOR
************************************************* DONE
*************************************************/

//Define a 1-bit full adder
module fulladd(sum, c_out, a, b, c_in);

// IO Port declaration
output sum, c_out;
input a, b, c_in;

//internal nets
wire s1, c1, c2;


//Instatiate Gate logic primitives
xor (s1, a, b);
and (c1, a, b);

xor (sum, s1, c_in);
and (c2, s1, c_in);

xor (c_out, c2, c1);

endmodule

// Define a 32-bit full adder
module fulladd32(sum, c_out, a, b, c_in);

// IO declarations
output [31:0] sum;
output c_out;
input [31:0] a, b;
input c_in;

//Internal Nets
wire [30:0] c0;

//instantiate the 1 bit full adders needed
//todo: refactor
fulladd fa0 (sum[0], c0[0], a[0], b[0], c_in);
fulladd fa1 (sum[1], c0[1], a[1], b[1], c0[0]);
fulladd fa2 (sum[2], c0[2], a[2], b[2], c0[1]);
fulladd fa3 (sum[3], c0[3], a[3], b[3], c0[2]);
fulladd fa4 (sum[4], c0[4], a[4], b[4], c0[3]);
fulladd fa5 (sum[5], c0[5], a[5], b[5], c0[4]);
fulladd fa6 (sum[6], c0[6], a[6], b[6], c0[5]);
fulladd fa7 (sum[7], c0[7], a[7], b[7], c0[6]);
fulladd fa8 (sum[8], c0[8], a[8], b[8], c0[7]);
fulladd fa9 (sum[9], c0[9], a[9], b[9], c0[8]);
fulladd fa10 (sum[10], c0[10], a[10], b[10], c0[9]);
fulladd fa11 (sum[11], c0[11], a[11], b[11], c0[10]);
fulladd fa12 (sum[12], c0[12], a[12], b[12], c0[11]);
fulladd fa13 (sum[13], c0[13], a[13], b[13], c0[12]);
fulladd fa14 (sum[14], c0[14], a[14], b[14], c0[13]);
fulladd fa15 (sum[15], c0[15], a[15], b[15], c0[14]);
fulladd fa16 (sum[16], c0[16], a[16], b[16], c0[15]);
fulladd fa17 (sum[17], c0[17], a[17], b[17], c0[16]);
fulladd fa18 (sum[18], c0[18], a[18], b[18], c0[17]);
fulladd fa19 (sum[19], c0[19], a[19], b[19], c0[18]);
fulladd fa20 (sum[20], c0[20], a[20], b[20], c0[19]);
fulladd fa21 (sum[21], c0[21], a[21], b[21], c0[20]);
fulladd fa22 (sum[22], c0[22], a[22], b[22], c0[21]);
fulladd fa23 (sum[23], c0[23], a[23], b[23], c0[22]);
fulladd fa24 (sum[24], c0[24], a[24], b[24], c0[23]);
fulladd fa25 (sum[25], c0[25], a[25], b[25], c0[24]);
fulladd fa26 (sum[26], c0[26], a[26], b[26], c0[25]);
fulladd fa27 (sum[27], c0[27], a[27], b[27], c0[26]);
fulladd fa28 (sum[28], c0[28], a[28], b[28], c0[27]);
fulladd fa29 (sum[29], c0[29], a[29], b[29], c0[28]);
fulladd fa30 (sum[30], c0[30], a[30], b[30], c0[29]);
fulladd fa31 (sum[31], c_out, a[31], b[31], c0[30]);

endmodule


// Define adder-subtractor
module addsub(sum, c_out, a, b, c_in);

// IO declarations
output [31:0] sum;
output c_out;
input [31:0] a, b;
input c_in;

//Internal Nets
wire [31:0] c1;

xor xrg [31:0] (c1, c_in, b);

fulladd32 fa1(sum, c_out, a, c1, c_in);

endmodule


/************************************************ STIMULUS 
*************************************************
*************************************************/

module stimulus;
reg [31:0] A, B;
reg [7:0] C_IN;
reg [4:0] const_shift;
wire [31:0] SUM;
wire zero, ovfl;
ALU al(SUM, zero, ovfl, A, B, const_shift, C_IN);
//initialize
initial
begin
$monitor ($time, " A= %b, B=%b, zero=%b, overflow=%b, C_IN= %d, SUM= %b\n",
A, B, zero, ovfl, C_IN, SUM);

end

//Stimulate Inputs
initial
begin
/*
0 - Addsub
1 2 3 - Shifter Unit ( 1 is the Cont'Var Control)
4 5 - Logic Unit
6 7 - Final Mux
{ 7 6 } { 5 4 } { 3 2 1 } { 0 } - Should Have done this in exact reverse.
*/
A = 32'd1021; B = 32'd50; const_shift = 5'b00001; C_IN = 0;

/*
Adder/Subtractor Add 0 PASS
*/
#1 $display("Set Less");
#5 C_IN[6] = 1; C_IN[7] = 0;

/*
Adder/Subtractor Add 0 PASS
Adder/Subtractor Subtract 1 PASS
*/
#1 $display("Adder/Subtractor");
#5 C_IN[6] = 0; C_IN[7] = 1;
#5 C_IN[0] = 1;

/*
Logic Unit AND 00 PASS
Logic Unit OR 01 PASS
Logic Unit XOR 10 PASS
Logic Unit NOR 11 PASS
*/
#1 $display("Logic Unit");
#5 C_IN[6] = 1; C_IN[7] = 1;
#5 C_IN[4] = 1; C_IN[5] = 0;
#5 C_IN[4] = 0; C_IN[5] = 1;
#5 C_IN[4] = 1; C_IN[5] = 1;

/*
Shifter No Shift 00 PASS
Shifter Logical Left 01 PASS
Shifter Logical Right 10 PASS
Shifter Arith Right 11 PASS
*/
#1 $display("Shifter Const");
#5 C_IN[6] = 0; C_IN[7] = 0;
#5 C_IN[1] = 0; C_IN[2] = 1; C_IN[3] = 0;
#5 C_IN[1] = 0; C_IN[2] = 0; C_IN[3] = 1;
#5 C_IN[1] = 0; C_IN[2] = 1; C_IN[3] = 1;

#1 $display("Shifter Var"); 
#1 B = 32'd2;
#5 C_IN[1] = 1; C_IN[2] = 0; C_IN[3] = 0;
#5 C_IN[1] = 1; C_IN[2] = 1; C_IN[3] = 0;
#5 C_IN[1] = 1; C_IN[2] = 0; C_IN[3] = 1;
#5 C_IN[1] = 1; C_IN[2] = 1; C_IN[3] = 1;

end

endmodule


/*********************************************** Tested
************************************************/

/****** ADD/SUBTRACT STIMULUS
//Stimulus module
module stimulus;

//Vars
reg [31:0] A, B;
reg C_IN;
wire [31:0] SUM;
wire C_OUT;

//instantiate the 32 bit full adder
//fulladd32 FA1_4(SUM, C_OUT, A, B, C_IN);

//instantiate adder/subtractor
addsub AS(SUM, C_OUT, A, B, C_IN);

//initialize
initial
begin
$monitor ($time, " A= %b, B=%b, C_IN= %b, --- C_OUT= %b, SUM= %b\n",
A, B, C_IN, C_OUT, SUM);

end

//Stimulate Inputs
initial
begin
A = 4'd0; B = 4'd0; C_IN = 1'b0;

#5 A = 4'd3; B = 4'd4;

#5 A = 4'd2; B=4'd5;

#5 A = 4'd9; B = 4'd9;

#5 A = 4'd10; B = 4'd15;

#5 A = 4'd2; B = 4'd4; C_IN = 1'b1;
end

endmodule

*/


/****** MUX STIMULUS
module stimulus;

// Vars to be connected to inputs

reg [31:0] IN0, IN1, IN2, IN3;
reg S1, S0;

// Dec output wire
wire [31:0] OUTPUT;

//instantiate the mux
mux32bit mymux(OUTPUT, IN0, IN1, IN2, IN3, S1, S0);

//define the module

initial
begin
// set input lines
IN0 = 4'd1; IN1 = 4'd2; IN2 = 4'd3; IN3 = 4'd4;
#1 $display("IN0= %b, IN1= %b, IN2= %b, IN3= %b\n",IN0,IN1,IN2,IN3);

//Choose IN0
S1 = 0; S0 = 0;
#1 $display("S1 = %b, S0 = %b, OUTPUT = %b\n", S1, S0, OUTPUT);

//Choose IN1
S1 = 0; S0 = 1;
#1 $display("S1 = %b, S0 = %b, OUTPUT = %b\n", S1, S0, OUTPUT);

//Choose IN2
S1 = 1; S0 = 0;
#1 $display("S1 = %b, S0 = %b, OUTPUT = %b\n", S1, S0, OUTPUT);

//Choose IN3
S1 = 1; S0 = 1;
#1 $display("S1 = %b, S0 = %b, OUTPUT = %b\n", S1, S0, OUTPUT);

end

endmodule
*/
